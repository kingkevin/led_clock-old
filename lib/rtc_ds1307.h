/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/** library to communicate with the Maxim DS1307 I2C RTC IC (API)
 *  @file rtc_ds1307.h
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2016
 *  @note user RAM is not handled
 *  @note peripherals used: I2C @ref rtc_ds1307_i2c, GPIO & timer @ref rtc_ds1307_square_wave_timer 
 */
#pragma once

/** @defgroup rtc_ds1307_i2c I2C peripheral used to communication with the DS1307 RTC IC
 *  @{
 */
/** I2C peripheral */
#define RTC_DS1307_I2C I2C1 /**< I2C peripheral */
#define RTC_DS1307_I2C_RCC RCC_I2C1 /**< I2C peripheral clock */
#define RTC_DS1307_I2C_PORT_RCC RCC_GPIOB /**< I2C I/O peripheral clock */
#define RTC_DS1307_I2C_PORT GPIOB /**< I2C I/O peripheral port */
#define RTC_DS1307_I2C_PIN_SDA GPIO_I2C1_SDA /**< I2C peripheral data pin (PB7) */
#define RTC_DS1307_I2C_PIN_SCL GPIO_I2C1_SCL /**< I2C peripheral clock pin (PB6) */
#define RTC_DS1307_I2C_ADDR 0x68 /**< DS1307 I2C address (fixed to 0b1101000) */
/** @} */

/** @defgroup rtc_ds1307_square_wave_timer timer peripheral used to count timer based on RTC IC square wave output
 *  @note comment out SQUARE_WAVE_TICS to not disable feature
 *  @{
 */
#define RTC_DS1307_SQUARE_WAVE_TICKS (RTC_DS1307_SQUARE_WAVE_FREQUENCY/256) /**< number of square wave tics before setting rtc_ds1307_tic_flag */
#define RTC_DS1307_SQUARE_WAVE_FREQUENCY 4096 /**< square wave output frequency from the RTC IC */
#define RTC_DS1307_SQUARE_WAVE_TIMER TIM2 /**< timer peripheral */
#define RTC_DS1307_SQUARE_WAVE_TIMER_RCC RCC_TIM2 /**< timer peripheral clock */
#define RTC_DS1307_SQUARE_WAVE_TIMER_IC TIM_IC1 /**< input capture channel (for TIM2_CH1) */
#define RTC_DS1307_SQUARE_WAVE_TIMER_IN TIM_IC_IN_TI1 /**< input capture input source (TIM2_CH1 becomes TI1, then TI1F, then TI1FP1) */
#define RTC_DS1307_SQUARE_WAVE_TIMER_TS TIM_SMCR_TS_IT1FP1 /**< input capture trigger (actually TI1FP1) */
#define RTC_DS1307_SQUARE_WAVE_TIMER_IRQ NVIC_TIM2_IRQ /**< timer interrupt */
#define RTC_DS1307_SQUARE_WAVE_TIMER_ISR tim2_isr /**< timer interrupt service routine */
#define RTC_DS1307_SQUARE_WAVE_GPIO_RCC RCC_GPIOA /**< timer port peripheral clock (TIM2_CH1 on PA0)*/
#define RTC_DS1307_SQUARE_WAVE_GPIO_PORT GPIOA /**< timer port (TIM2_CH1 on PA0) */
#define RTC_DS1307_SQUARE_WAVE_GPIO_PIN GPIO_TIM2_CH1_ETR /**< timer pin input, connect to RTC IC square wave output (TIM2_CH1 on PA0) */
/** @} */

#if defined(RTC_DS1307_SQUARE_WAVE_TICKS)
extern volatile uint32_t rtc_ds1307_ticks; /**< increment on SQUARE_WAVE_TICS square wave ticks */
extern volatile bool rtc_ds1307_tick_flag; /**< set on SQUARE_WAVE_TICS square wave ticks */
#endif

/** setup communication with RTC IC
 *  configure the I2C port defined in the sources
 */
void rtc_ds1307_setup(void);
/** verify if oscillator is disabled
 *  @return if oscillator is disabled
 */
bool rtc_ds1307_oscillator_disabled(void);
/** read square wave output frequency (in Hz)
 *  @return square wave output frequency in Hz, 0 if disabled
 */
uint16_t rtc_ds1307_read_square_wave(void);
/** read seconds from RTC IC
 *  @return number of seconds (0-59) of the current time
 */
uint8_t rtc_ds1307_read_seconds(void);
/** read minutes from RTC IC
 *  @return number of minutes (0-59) of the current time
 */
uint8_t rtc_ds1307_read_minutes(void);
/** read hours from RTC IC
 *  @return number of hours (0-23) of the current time
 */
uint8_t rtc_ds1307_read_hours(void);
/** read day from RTC IC
 *  @return day of the week (1-7, 1 is Sunday) of the current time, 1 being Sunday
 */
uint8_t rtc_ds1307_read_day(void);
/** read date from RTC IC
 *  @return day of the month (1-31) of the current time
 */
uint8_t rtc_ds1307_read_date(void);
/** read month from RTC IC
 *  @return month of the year (1-12) of the current time
 */
uint8_t rtc_ds1307_read_month(void);
/** read year from RTC IC
 *  @return year of the century (00-99) of the current time
 */
uint8_t rtc_ds1307_read_year(void);
/** read time from RTC IC
 *  @return array of {seconds, minutes, hours, day, date, month, year} as defined above
 */
uint8_t* rtc_ds1307_read_time(void);
/** disable RTC IC oscillator
 *  @return if disabling oscillator succeeded
 */
bool rtc_ds1307_oscillator_disable(void);
/** enable RTC IC oscillator
 *  @return if enabling oscillator succeeded
 */
bool rtc_ds1307_oscillator_enable(void);
/** write square wave output frequency (in Hz)
 *  @param[in] frequency square wave output frequency in Hz (0 to disable, 1, 4096, 8192, 32768)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_square_wave(uint16_t frequency);
/** write seconds into RTC IC
 *  @param[in] seconds number of seconds (0-59)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_seconds(uint8_t seconds);
/** write minutes into RTC IC
 *  @param[in] minutes number of minutes (0-59)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_minutes(uint8_t minutes);
/** write hours into RTC IC
 *  @param[in] hours number of hours (0-23)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_hours(uint8_t hours);
/** write day into RTC IC
 *  @param[in] day day of the week (1-7, 1 is Sunday)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_day(uint8_t day);
/** write date into RTC IC
 *  @param[in] date day of the month (1-31)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_date(uint8_t date);
/** write month into RTC IC
 *  @param[in] month month of the year (1-12)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_month(uint8_t month);
/** write year into RTC IC
 *  @param[in] year year of the century (00-99)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_year(uint8_t year);
/** write time into RTC IC
 *  @param[in] seconds number of seconds (0-59)
 *  @param[in] minutes number of minutes (0-59)
 *  @param[in] hours number of hours (0-23)
 *  @param[in] day day of the week (1-7, 1 is Sunday)
 *  @param[in] date day of the month (1-31)
 *  @param[in] month month of the year (1-12)
 *  @param[in] year year of the century (00-99)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_time(uint8_t seconds, uint8_t minutes, uint8_t hours, uint8_t day, uint8_t date, uint8_t month, uint8_t year);
