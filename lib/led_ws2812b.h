/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/** library to drive a WS2812B LED chain (API)
 *  @file led_ws2812b.h
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2016
 *  @note peripherals used: SPI @ref led_ws2812b_spi, timer @ref led_ws2812b_timer, DMA @ref led_ws2812b_dma
 */
#pragma once

/** number of LEDs on the WS2812B strip */
#define LED_WS2812B_LEDS 48

/** peripheral configuration */
/** @defgroup led_ws2812b_spi SPI peripheral used to control the WS2812B LEDs
 *  @{
 */
#define LED_WS2812B_SPI SPI1 /**< SPI peripheral */
#define LED_WS2812B_SPI_DR SPI1_DR /**< SPI data register for the DMA */
#define LED_WS2812B_SPI_RCC RCC_SPI1 /**< SPI peripheral clock */
#define LED_WS2812B_SPI_PORT_RCC RCC_GPIOA /**< SPI I/O peripheral clock */
#define LED_WS2812B_SPI_PORT GPIOA /**< SPI port */
#define LED_WS2812B_SPI_CLK GPIO_SPI1_SCK /**< SPI clock pin (PA5), connect to PWM output */
#define LED_WS2812B_SPI_DOUT GPIO_SPI1_MISO /**< SPI data pin (PA6), connect to WS2812B DIN */
/** @} */
/** @defgroup led_ws2812b_timer timer peripheral used to generate SPI clock
 *  @{
 */
#define LED_WS2812B_TIMER TIM3 /**< timer peripheral */
#define LED_WS2812B_TIMER_RCC RCC_TIM3 /**< timer peripheral clock */
#define LED_WS2812B_TIMER_OC TIM_OC3 /**< timer output compare used to set PWM frequency */
#define LED_WS2812B_CLK_RCC RCC_GPIOB /**< timer port peripheral clock */
#define LED_WS2812B_CLK_PORT GPIOB /**< timer port */
#define LED_WS2812B_CLK_PIN GPIO_TIM3_CH3 /**< timer pin to output PWM (PB0), connect to SPI clock input */
/** @} */
/** @defgroup led_ws2812b_dma DMA peripheral used to send the data
 *  @{
 */
#define LED_WS2812B_DMA DMA1 /**< DMA peripheral to put data for WS2812B LED in SPI queue (only DMA1 supports SPI1_TX interrupt) */
#define LED_WS2812B_DMA_RCC RCC_DMA1 /**< DMA peripheral clock */
#define LED_WS2812B_DMA_CH DMA_CHANNEL3 /**< DMA channel (only DMA1 channel 3 supports SPI1_TX interrupt) */
#define LED_WS2812B_DMA_IRQ NVIC_DMA1_CHANNEL3_IRQ /**< DMA channel interrupt signal */
#define LED_WS2812B_DMA_ISR dma1_channel3_isr /**< DMA channel interrupt service routine */
/** @} */

/** setup WS2812B LED driver */
void led_ws2812b_setup(void);
/** set color of a single LED
 *  @param[in] led the LED number to set the color
 *  @param[in] red the red color value to set on the LED
 *  @param[in] green the green color value to set on the LED
 *  @param[in] blue the blue color value to set on the LED
 *  @note transmission needs to be done separately
 */
void led_ws2812b_set_rgb(uint16_t led, uint8_t red, uint8_t green, uint8_t blue);
/** transmit color values to WS2812B LEDs
 *  @return true if transmission started, false if another transmission is already ongoing
 */
bool led_ws2812b_transmit(void);
