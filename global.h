/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/** global definitions and methods
 *  @file global.h
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2016
 */
#pragma once
#include <libopencm3/stm32/gpio.h> // GPIO defines
#include <libopencm3/cm3/nvic.h> // interrupt defines
#include <libopencm3/stm32/exti.h> // external interrupt defines

/** get the length of an array */
#define LENGTH(x) (sizeof(x) / sizeof((x)[0]))

/** @defgroup board_led board LED GPIO
 *  @{
 */
#if defined(SYSTEM_BOARD)
/* on system board LED is on pin 11/PA1 */
#define LED_RCC RCC_GPIOA /**< GPIO peripheral clock (port A on system board) */
#define LED_PORT GPIOA /**< GPIO port (port A on system board) */
#define LED_PIN	GPIO1 /**< GPIO pin (pin PA1 on system board) */
#elif defined(BLUE_PILL)
/* on minimum system LED is on pin 2/PC13 */
#define LED_RCC RCC_GPIOC /**< GPIO peripheral clock (port C on blue pill) */
#define LED_PORT GPIOC /**< GPIO port (port C on blue pill) */
#define LED_PIN GPIO13 /**< GPIO pin (pin PC13 on system board) */
#elif defined (MAPLE_MINI)
/* on maple mini LED is on pin 19/PB1 */
#define LED_RCC RCC_GPIOB /**< GPIO peripheral clock (port B on maple mini) */
#define LED_PORT GPIOB /**< GPIO port (port B on maple mini) */
#define LED_PIN GPIO1 /**< GPIO pin (pin PB1 on maple mini) */
#endif
/** @} */

/** @defgroup board_button board user button GPIO
 *  @{
 */
#if defined(MAPLE_MINI)
/* on maple mini user button is on 32/PB8 */
#define BUTTON_RCC RCC_GPIOB /**< GPIO peripheral clock */
#define BUTTON_PORT GPIOB /**< GPIO port */
#define BUTTON_PIN GPIO8 /**< GPIO pin */
#define BUTTON_EXTI EXTI8 /**< GPIO external interrupt */
#define BUTTON_IRQ NVIC_EXTI9_5_IRQ /**< GPIO line interrupt (interrupt for line 9 to 5 for pin) */
#define BUTTON_ISR exti9_5_isr /**< GPIO line interrupt service routine (isr for line 9 to 5 for pin 8) */
#endif
/** @} */

/** switch on board LED */
inline void led_on(void)
{
#if defined(SYSTEM_BOARD) || defined(BLUE_PILL)
	gpio_clear(LED_PORT, LED_PIN);
#elif defined(MAPLE_MINI)
	gpio_set(LED_PORT, LED_PIN);
#endif
}
/** switch off board LED */
inline void led_off(void)
{
#if defined(SYSTEM_BOARD) || defined(BLUE_PILL)
	gpio_set(LED_PORT, LED_PIN);
#elif defined(MAPLE_MINI)
	gpio_clear(LED_PORT, LED_PIN);
#endif
}
/** toggle board LED */
inline void led_toggle(void)
{
	gpio_toggle(LED_PORT, LED_PIN);
}
/** default printf output */
int _write(int file, char *ptr, int len);
/** get binary representation of a number
 *  @param[in] binary number to represent in binary
 *  @param[in] rjust justify representation with leading zeros
 *  @return string with binary representation of the number
 */
char* b2s(uint64_t binary, uint8_t rjust);

