The LED clock is an add-on for round wall clocks.
The purpose is to have LEDs on the circumference of the clock to show the progress of the time using coloured light.

For that you will need:

- a WS2812B RGB LEDs strip (long enough to go around the clock)
- a development board with a STM32F103 micro-controller equipped with a 32.768 kHz oscillator for the Real Time Clock (such as the [blue pill](https://wiki.cuvoodoo.info/doku.php?id=stm32f1xx#blue_pill)), or using a external [Maxim DS1307](https://www.maximintegrated.com/en/products/digital/real-time-clocks/DS1307.html) RTC module
- a coin cell battery to keep the RTC running (optional)
- a GL5528 photo-resistor to adjust the LED brightness (optional)
- a DCF77 module to set and update the time automatically (salvaged from a radio controlled digital clock)

project
=======

summary
-------

The time will be shown as arc progress bars, in addition to the original hands of the clock pointing at the current time.
The hours passed since the beginning of the midday are shown using blue LEDs.
The minutes passed sine the beginning of the hour are shown using green LEDs.
Whichever progress is higher will be shown on top of the other.
For example if it's 6:45, the first half of the circle will be blue, and an additional quarter will be green.
The seconds passed since the beginning of the minute are shown using a running red LED, similar to the seconds hand.
The red colour might be added on top of the blue, or green colour, then showing as violet or orange.
The (gamma corrected) brightness of the last LED shows how much of the hour, minute, or second has passed.


technology
----------

The brain of this add-on is a [STM32 F1 series micro-controller](http://www.st.com/web/en/catalog/mmc/FM141/SC1169/SS1031) (based on an ARM Cortex-M3 32-bit processor).

To keep track of the time a Real Time Clock (RTC) is used.
If the board includes a 32.768 kHz oscillator (such as a [blue pill](https://wiki.cuvoodoo.info/doku.php?id=stm32f1xx#blue_pill)) the micro-controller will use the internal RTC.
Otherwise connect an external [Maxim DS1307](https://www.maximintegrated.com/en/products/digital/real-time-clocks/DS1307.html) RTC module to the I2C port and set `EXTERNAL_RTC` in `main.c` to `true`.
Also connect the external RTC square wave output in order to have a sub-second time precision.

Connect a DCF77 module (e.g. salvaged from a radio controlled clock) to the micro-controller.
This will allow to automatically get precise time (at least in Europe) when booting.
Since the RTC is drifting, the time will get updated using DCF77 every hour to keep <0.5 s time precision.
Alternatively set the time using serial over the USB port (providing the CDC ACM profile) or USART port and enter "time HH:MM:SS".

Power the board using an external 5 V power supply (e.g. through the USB port).
This will power the micro-controller, and the LEDs (a single LED consumes more energy than the micro-controller).
To keep the correct time in case the main power supply gets disconnected optionally connect a 3 V coin battery on the VBAT pin for the internal RTC, or in the module for the external RTC.

For the LEDs use a 1 meter LED strip with 60 red-green-blue WS2812B LEDs.
Tape the LED strip along the border/edge of the clock.
Ideally the wall clock has a diameter of 32 cm for a 1 m LED strip to completely fit.
Otherwise change the number of actually used LEDs in the source files.
Connect the 5 V power rail of the LED strip to the 5 V pin of the board.
Connect the DIN signal line of the LED strip to the MISO pin of the micro-controller on PA6.
SPI is used to efficiently shift out the LED colour values to the WS2812B LEDs.
A custom clock is provided for this operation using channel 3 of timer 3 on pin PB0.
Simply connect this clock to the SPI CLK input on pin PA5.

The brightness of the LEDs is dependant on the ambient luminance.
To measure the ambient luminance a GL5528 photo-resistor is used.
Connect one leg of the photo-resistor to ADC channel 1 and the other to ground.
Connect one leg of a 1 kOhm resistor to ADC channel 1 and the other to a 3.3 V pin.
This voltage divider allows to measure the photo-sensor's resistance and determine the luminance.
If you don't want to use this feature, connect PA1 to ground for the highest brightness or Vcc for the lowest brightness.

board
=====

The current implementation uses a [blue pill](https://wiki.cuvoodoo.info/doku.php?id=stm32f1xx#blue_pill).

The underlying template also supports following board:

- [Maple Mini](http://leaflabs.com/docs/hardware/maple-mini.html), based on a STM32F103CBT6
- [System Board](https://wiki.cuvoodoo.info/doku.php?id=stm32f1xx#system_board), based on a STM32F103C8T6
- [blue pill](ihttps://wiki.cuvoodoo.info/doku.php?id=stm32f1xx#blue_pill), based on a STM32F103C8T6

**Which board is used is defined in the Makefile**.
This is required:

- for the linker script to know the memory layout (flash and RAM)
- map the user LED and button provided on the board

connections
===========

Connect the peripherals the following way (STM32F10X signal; STM32F10X pin; peripheral pin; peripheral signal; comment):

- USART1_TX; PA9; RX; UART RX; optional, same as over USB ACM
- USART1_RX; PA10; TX; UART TX; optional, same as over USB ACM
- I2C1_SDA; PB7; DS1307 SDA; SDA; optional, when using external RTC
- I2C1_SCL; PB6; DS1307 SCL; SCL; optional, when using external RTC
- TIM2_CH1_ETR; PA0; DS1307 SQ; square wave output; optional, when using external RTC
- ADC12_IN1; PA1; GL5528; photo-resistor + 1 kOhm to 3.3 V; without GL5528 photo-resistor connect to ground for highest brightness or Vcc for lowest brightness
- TIM3_CH3; PB0; PA5; SPI1_SCK; generated clock for WS2812B transmission
- SPI1_MISO; PA6; WS2812B DIN; DIN; WS2812B LED strip data stream
- GPIO; PA2; DCF77 PO; \#EN; DCF77 enable on low
- GPIO; PA3; DCF77 TN; DCF77; DCF77 high bit pulses

All pins are configured using `define`s in the corresponding source code.

code
====

dependencies
------------

The source code uses the [libopencm3](http://libopencm3.org/) library.
The projects is already a git submodules.
To initialize and it you just need to run once: `git submodule init` and `git submodule update`.

firmware
--------

To compile the firmware run `make`.

documentation
-------------

To generate doxygen documentation run `make doc`.

flash
-----

The firmware will be flashed using SWD (Serial Wire Debug).
For that you need an SWD adapter.
The `Makefile` uses a ST-Link V2 along OpenOCD software (per default), or a Black Magic Probe.
To flash using SWD run `make flash`.

debug
-----

SWD also allows to debug the code running on the micro-controller using GDB.
To start the debugging session run `make debug`.

USB
---

The firmware offers serial communication over USART1 and USB (using the CDC ACM device class).

You can also reset the board by setting the serial width to 5 bits over USB.
To reset the board run `make reset`.
This only works if the USB CDC ACM is running correctly and the micro-controller isn't stuck.
